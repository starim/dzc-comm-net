Dropzone Commander Comm Net
==

This is a simple toolkit to create invites to your friends to play Dropzone
Commander scenarios. The idea is that you come up with a fun scenario you want
to play, write up a briefing addressed to your friend, and send this web page
to get your friend interested. The page simulates receiving an urgent briefing
from UCM headquarters on a dire situation that has developed, with details
filled in by you. Currently the web page works best for UCM players but themes
for other factions could easily be made by anyone who understands HTML and CSS.
Using the stock theme and animations requires only rudimentary knowledge of
HTML.


Creating your own scenario briefing
==

Want to try this for yourself? The easiest way to create and share your own
briefing is to grab the compiled comm-net.html file from [the GitHub releases
for this project](https://github.com/starim/dzc-comm-net/releases). This is a
stand-alone file so when you're done editing it you can send it to your friends
via email, USB stick, or however you like to transfer files. Your friends only
has to save it to their computers and open it with an up-to-date browser
(Firefox and Chrome have been tested and are known to work, but Internet
Explorer may not due to the advanced CSS animations).

If you want to edit any of the scripts and CSS used on the page or add your own
new effects, then you can customize the source files in this project and
compile your own standalone HTML page by base64-encoding each resource file and
embedding it in the HTML. Or if you have a web server, you can skip the
base64-encoding step and simply host all the files on your server like any
other static web page.


Walkthrough
==

This is a walkthrough for setting up your own briefing using the compiled
comm-net.html file that you can download from the project's [releases
page](https://github.com/starim/dzc-comm-net/releases).


Invent a Battle Scenario
--

Invent a scenario you want to play with your friends, or pick one from the
Dropzone Commander manual. Devise a backstory for the conflict so you can write
a good briefing to get your friends excited to play.


(Optional) Customize the introductory animation
--

The introductory animation shown on page load adds flavor to the scenario
briefing. It pretends to be connecting through a UCM comm relay to UCM
headquarters, then alerts the viewer that something terrible has happened, all
UCM forces are on high alert, and there are urgent orders waiting. Finally it
prompts the viewer for a password (more on that in the next section) and when
the password is entered the scenario briefing is revealed.

Customizing this section

In comm-net.html, the section with id **intro-aria** contains all the
individual parts of the introductory animation sequence. All child elements of
the intro-aria section will be displayed in sequence, with the order determined
by the id given to the elements. Thus the element with id **intro-stage-1** is
displayed first, then is hidden and **intro-stage-2** is shown, and so on. The
*data-delay* property of the animation elements gives the time in seconds at
which each element becomes visible and the previous one faded out. This delay
counts from the start of the whole animation sequence.

If you add or remove elements in the animation sequence, remember to reorder
the numbering on the ids. The id numbers must be sequential with no duplicate
ids. Also, the last animation element must always contain the password prompt
(more on this in the next section).

See the following section before changing the animation element with the
password input and remember that the password prompt has to be the last
animation element in the sequence if you choose to keep it.

Styles

The following CSS classes can be applied to give an animation element some
additional styles:

* intro-success: colors the text green
* intro-alert: colors the text red and adds bars above and below the element to
  frame it


Set the "password"
--

At the end of the intoductory animation, the viewer is prompted for a password.
This is a little bit of fluff to engage the viewer in the experience and I had
a lot of fun picking in-jokes as passwords my friends would have to type in.

The password is set by the *data-password* property of the input field with id
**intro-password**. Change it to whatever you like, but avoid using "
characters without escaping them with a backslash first (thus a password of
**pass"word** should be typed in the *data-password* field as **pass\"word**).

**NOTE** - since the correct password is embedded in the HTML and the intro
animation can be skipped by pressing the escape key anyway, this password
prompt is purely a joke. Don't depend on it if you type things in the briefing
you don't want anyone but the intended recipient to see.


Write your briefing
--

Everything within the section with id **message-body** is what the user sees as
part of the actual briefing. The sample briefing is straightforward with only a
header and five paragraphs. If you'd like to keep your briefing quick and easy
simply replace the text in the existing paragraphs with your own text.

If you'd like something more fancy and know some HTML, go ahead and replace the
paragraphs with whatever other elements you like to bring your scenario to
life--any HTML you like is valid here.

The &lt;header&gt; element is given some extra styles to make it stand out, and is
used in the stock briefing to delcare that the message is from a high-ranking
UCMA officer. You can remove the header if you like, or just change the text
inside it.

When writing your briefing you'll want to refresh the page fairly often to see
your updates reflected on the page. To skip the introductory animation and go
straight to the briefing, pressing the escape key will immediately skip the
intro animation.


Distribute your briefing
--

Now all you have to do is send your recipient the edited file comm-net.html
file. They'll need an up-to-date version of Firefox or Chrome to view the page
correctly. Other browsers may work but haven't been tested.

