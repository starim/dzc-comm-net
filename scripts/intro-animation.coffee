# this script controls the introductory animation sequence shown before the
# user gets to see the main content
$(document).ready ->

  audio_dialup = $("#audio-dialup").get(0)
  audio_dialup.volume = 0.8

  # stores the ID of the timer that fires that the next animation in the
  # sequence
  next_transition_timer = null

  # cache the elements that make up the animation
  intro_elements = $("#intro-aria > *")

  # call this function when it's time to stop the animation and show the
  # content it's hiding; this function is idempotent
  complete_animation = ->
    clearTimeout next_transition_timer
    intro_elements.removeClass "active"
    audio_dialup.pause()
    intro_aria = $("#intro-aria")
    if not intro_aria.hasClass "closing"
      intro_aria.off "keyup"
      intro_aria.animate {width: 'toggle'}
    intro_aria.addClass "closing"

  # the number of seconds from the start of the animation to the previous
  # animation transition
  timer_to_previous_transition_sec = 0.0 

  # the user can press the escape key to skip this whole animation
  $(document).keyup (event) ->
    if event.keyCode == 27
      complete_animation()

  max_stage = intro_elements.length
  animate_next = (current_stage, total_elapsed_time_ms) ->

    current_stage += 1

    $("#intro-stage-#{current_stage - 1}").removeClass "active"
    $("#intro-stage-#{current_stage}").addClass "active"

    if current_stage < max_stage

      # total elapsed time in milliseconds for the entire animation sequence
      # when the next transition triggers
      next_transition_ms =
        (1000 * $("#intro-stage-#{current_stage + 1}").data "delay")

      # milliseconds until the next scheduled animation transition
      time_to_next_transition_ms = next_transition_ms - total_elapsed_time_ms

      next_transition_timer = setTimeout ( ->
        animate_next current_stage, next_transition_ms
      ), time_to_next_transition_ms
  
  # start the introductory animation sequence
  next_transition_timer = setTimeout ( ->
    audio_dialup.play()
    animate_next(0, 0)
  ), 2000


  # when the user enters the correct password, remove the aria hiding the
  # main content
  password_input = $("#intro-password")
  password = password_input.data "password"
  password_input.on "keyup", (event) ->
    if $(this).val() == password
      complete_animation()

