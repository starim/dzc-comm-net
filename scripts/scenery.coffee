# this script controls the pictures of scenery
$(document).ready ->

  # how long the animation transitioning one image to another should last, in
  # milliseconds
  TRANSITION_ANIMATION_DURATION = 1200

  # how long to show each image in milliseconds while cycling images in a
  # series
  SHOW_IMAGE_DURATION = 3500


  # Stores a reference any active timer so it can be cancelled.
  active_timer = null


  # display the scenery images
  $("#show-scenery").click ->
    $("#scenery").animate { width: 'show'} , { complete: ->
      cycle_scenery_in_series 0, 0, null
    }


  # Returns true if a new image series was selected by the user, false
  # otherwise.
  check_if_new_series_selected = (current_series_id) ->
    $("#scenery-controls .selected[for='scenery-display-#{current_series_id}']").
      length == 0

  # Takes care of handling setting up visuals elements other than the images
  # themselves when a new series gets selected.
  select_new_series = (new_series_id) ->
    # apply visual styles to the selected series' button and remove them from
    # the previous series' button
    $("#scenery-controls label.selected").removeClass("selected")
    $("#scenery-controls label[for='scenery-display-#{new_series_id}']").
      addClass("selected")

    # show the new series' caption while hiding the previous series' caption
    $("#scenery-image-container > p:visible").
      fadeOut TRANSITION_ANIMATION_DURATION
    $("#scenery-image-container > #desc-#{new_series_id}").
      fadeIn TRANSITION_ANIMATION_DURATION


  # Start cycling through images in the given series.
  #   - series_id is the ID number of the series to cycle through
  #   - image_id is the ID number of the image in the series
  #   - previous_image is the jQuery DOM element for the currently displayed image,
  #     or null if no image is currently displayed
  cycle_scenery_in_series = (series_id, image_id, previous_image) ->

    # clear the timer, just in case another timer was running
    clearTimeout active_timer

    # did a new series get selected?
    if check_if_new_series_selected(series_id)
      select_new_series(series_id)

    next_image = get_image(series_id, image_id)

    # if we've reached the end of the images in this series, go back to the
    # first
    if next_image.length == 0
      image_id = 0
      next_image = get_image(series_id, image_id)

    if previous_image
      previous_image.fadeOut TRANSITION_ANIMATION_DURATION

    next_image.fadeIn TRANSITION_ANIMATION_DURATION, ->
      active_timer = setTimeout( ( ->
          cycle_scenery_in_series series_id, image_id + 1, next_image
        ),
        SHOW_IMAGE_DURATION
      )


  # Return the image with the given series and image ID numbers.
  get_image = (series_id, image_id) ->
    $("#scenery-image-container ##{series_id}-#{image_id}")


  # if a new series is selected, stop the current image cycling and start
  # cycling a new series
  $("#scenery-controls > input").change (eventData) ->

    series_id = parseInt(eventData.target.value)

    if(
      $("#scenery-controls .selected[for='scenery-display-#{series_id}']").
        length == 0
    )
      cycle_scenery_in_series(
        series_id,
        0,
        $("#scenery-image-container img:visible")
      )


  $("#hide-scenery").click ->
    clearTimeout active_timer
    $("#scenery").animate { width: 'hide'}

